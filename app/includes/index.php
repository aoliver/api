<?php
    //set headers
    header('X-Frame-Options: DENY');
    header('X-Content-Type-Options: nosniff');

    //forbidden
    http_response_code(403);

    //this blank index file is to prevent directory listing when mod rewrite is not active
?>