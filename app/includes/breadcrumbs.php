<?php
    //breadcrumbs class
    class breadcrumb{
        var $pathsubroute="",
        $breadcrumbArray=array(),
        $thisPage = array(),
        $hashBangs = array(),
        $page_count = 0;
        
        function __construct($theInput = false){
            $i = 0;

            $input_url = $_SERVER['REQUEST_URI'];
            $add_in = '';

            if(defined('PATH_SITE') && strlen(PATH_SITE) > 0){
                $add_in = preg_replace('/^\/|\/$/', '', PATH_SITE).'/';
                $input_url = str_replace($add_in, '', $input_url);
            }

            $requestURI = explode('/', $input_url);
            
            
            if($theInput){
                $requestURI = explode('/', $theInput);
            }
            $arrSubDir=array_slice($requestURI,2,count($requestURI));
            $arrSiteDIR=array_slice($requestURI,1,count($requestURI));
            
            foreach($arrSubDir as $inpRoute){$this->pathsubroute.="../";}
            
            $pSub = "";
            $pNsubLnk = "";
            foreach($arrSiteDIR as $pge){               
                
                $pSub.="../";
                
                //remove query information
                $qSplit = explode("?",$pge);
                $pName = str_replace("%20"," ",$qSplit[0]);

                if(strlen($pName) > 0){
                    if($pNsubLnk !== ""){
                        $pNsubLnk.= "/".$pName;
                    } else{
                        $pNsubLnk.= $pName;
                    }
                    
                    $this->breadcrumbArray[] = array(
                        "name" => $pName,
                        "title" => str_replace('_', ' ', $pName),
                        "link" => $pSub.$pName,
                        "internal_link" => $add_in.$pNsubLnk
                    );
                }
            }

            if( count( $this->breadcrumbArray ) > 0 ){ $this->thisPage = $this->breadcrumbArray[ count( $this->breadcrumbArray ) -1 ]; }
            else{ $this->thisPage = (isset($this->breadcrumbArray[0]) ? $this->breadcrumbArray[0] : []); }

            $this->page_count = (isset($this->breadcrumbArray[0]) && strlen($this->breadcrumbArray[0]['name']) > 0 ? count($this->breadcrumbArray) : 0);
        }

        public function return_paths(){
            return $this->breadcrumbArray;
        }

        public function return_path($id = 0){
            return (isset($this->breadcrumbArray[$id]) ? $this->breadcrumbArray[$id] : false);
        }
    
    }
?>