<?php
/*
    PHP Dataset Class
    Copyright (C) 2019 Alex Oliver

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    @version: 1.0.0
    @environment: Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/php-dataset-class
*/
    class arrDB{
        
        var $dataset,
        $db_file;

        const version = 'arrDB v1.0.0 Aug 2019';
        const prefix_dataset = 'dset_';

        function __construct($db_file = false, $auto_create = true){
            if(is_string($db_file) && strlen($db_file) > 0){
                
                $this->db_file = $db_file;

                //create empty database
                if(!file_exists($db_file) && $auto_create){
                    $this->dataset = $this->init_datadabse();
                    $this->update_database();
                } else {
                    include $db_file;
                    if(!$this->dataset = (isset($dataset) ? json_decode($dataset, true) : false)){
                        die('dataset not found');
                    }
                }
            }
        }

        //init new database
        private function init_datadabse(){
            return [
                'init_version' => $this::version,
                'created' => time(),
                'datasets' => []
            ];
        }

        //create a new dataset
        public function create_dataset($dataset_name = false, $content_array = false){
            $ds_name = (is_string($dataset_name) ? $this::prefix_dataset.$dataset_name : false);
            if($ds_name && !$this->data_set_exists($dataset_name)){
                $this->dataset[$ds_name] = (is_array($content_array) ? $content_array : []);
                $this->update_database();
                return $this;
            }
            return $this;
        }

        //does dataset exist
        public function data_set_exists($dataset_name = false, $return_set = false){
            $dataset_name = (is_string($dataset_name) ? $this::prefix_dataset.$dataset_name : false);
            return ($dataset_name && isset($this->dataset[$dataset_name]) ? ($return_set ? $this->dataset[$dataset_name] : true) : false);
        }

        //get record from key id
        public function get_record_from_key($dataset_name = false, $record_id = false){
            $dataset_name = (is_string($dataset_name) ? $this::prefix_dataset.$dataset_name : false);
            if($dataset_name && is_string($record_id) && strlen($record_id) > 0){
                return (isset($this->dataset[$dataset_name]) && isset($this->dataset[$dataset_name][$record_id]) ? $this->dataset[$dataset_name][$record_id] : false);
            }
        }

        //update database
        public function update_database(){
            $this->dataset = array_merge([
                'updated' => time(),
                'current_version' => $this::version
            ], $this->dataset);

            $file_content = '<?php $dataset = \''.json_encode($this->dataset).'\'; ?>';

            file_put_contents($this->db_file, $file_content);

            return $this;
        }
    }
?>