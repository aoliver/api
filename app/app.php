<?php
    class app{
        

        protected const path_includes = __DIR__.'/includes/';
        protected const path_datasets = __DIR__.'/datasets/';

        var $breadcrumbs,
        $return_messages,
        $posted_body;

        public function init_api(){
            //set json header
            header('Content-Type: application/json');

            //set file name
            header('Content-disposition: inline; filename="api-call-'.time().'.json"');

            //return messages
            $this->load_messages();

            //includes
            $this->includes();

            //new breadcrumb instance
            $this->breadcrumbs = new breadcrumb;

            //load and check IP database
            $this->init_ip_database()->check_ip();

            //load keys database
            $this->init_keys_database();

            //posted body cURL or AJAX requests
            try {
                $this->posted_body = json_decode(file_get_contents('php://input'), true);   
            } catch (Exception $e) {
                $this->render_message($this->return_messages['Invalid JSON'], 'Core', true, file_get_contents('php://input'));
            }

            //check api key
            $this->check_api_key();

            //execute service
            $this->execute_service();

            //render message
            $this->render_message($this->return_messages['Missing service'], 'Core');
        }

        //include files
        private function includes(){
            if(is_dir($this::path_includes) && $include = scandir($this::path_includes)){
                foreach($include as $this_file){
                    if(!in_array($this_file, ['.', '..', 'index.php']) && substr($this_file, -4) === '.php'){
                        include_once $this::path_includes.$this_file;
                    }
                }
            }
        }

        private function check_api_key(){

            //new keys database
            $keys_db = new arrDb($this::path_datasets."keys.php");
            
            //check for key in database
            if(isset($this->posted_body['key']) && $key_reocrd = $keys_db->get_record_from_key('keys', $this->posted_body['key'])){
                return (isset($key_reocrd['enabled']) ? $key_reocrd['enabled'] : false);
            }

            //render message
            $this->render_message($this->return_messages['Invalid key'], 'Core', true, $this->posted_body);
        }

        //check ip
        private function check_ip(){
            //new keys database
            $ips_db = new arrDb($this::path_datasets."ips.php");

            //server ip
            $this_server_ip = $_SERVER['REMOTE_ADDR'];

            //check for key in database
            if($ip_reocrd = $ips_db->get_record_from_key('ips', $this_server_ip)){
                return (isset($ip_reocrd['enabled']) ? $ip_reocrd['enabled'] : false);
            }

            //render message
            $this->render_message($this->return_messages['IP now allowed'], 'Core', true, $this->posted_body);
        }

        //init keys database
        private function init_keys_database(){
            $keys_db = new arrDb($this::path_datasets."keys.php");

            $keys_db->create_dataset('keys', [
                API_MATSER_KEY => [
                    'enabled' => true,
                    'added' => time()
                ] 
            ]);

            return $this;
        }


        //init ip database
        private function init_ip_database(){
            $keys_db = new arrDb($this::path_datasets."ips.php");

            $keys_db->create_dataset('ips', [
                $_SERVER['REMOTE_ADDR'] => [
                    'enabled' => true,
                    'added' => time()
                ] 
            ]);

            return $this;
        }

        //execute action
        private function execute_service(){
            if($service = $this->breadcrumbs->return_path(0)){
                $service_file = PATH_SERVICES.'/'.$service['name'].'/service.php';
                if(file_exists($service_file)){
                    include_once $service_file;
                    die;
                }

                $this->render_message($this->return_messages['Service not found'], 'Core', true, [
                    'service' => $service['name']
                ]);
            }
        }

        //return messages
        protected function load_messages(){
            include __DIR__.'/return-messages.php';
            $this->return_messages = $return_messages;
        }

        //render post message and kill app
        protected function render_message($json_data, $section = 'core', $is_error = true, $additional_data = []){
            $message = [
                'application' => API_TITLE,
                'service' => $section,
                'is_error' => $is_error,
                'request_date' => date('c'),
                'message' => $json_data,
                'return_data' => $additional_data
            ];

            echo json_encode($message);
            die;
        }
    }

    //new app instance
    $api = new app;
    $api->init_api();
?>