<?php
    //return codes and messages
    $return_messages = [
        'Nothing to return' => 'Nothing to return',
        'Missing service' => 'Missing API service',
        'Service not found' => 'Service could not be found',
        'Invalid key' => 'Missing or invalid API key',
        'Return Success' => 'API return success',
        'Invalid JSON' => 'Missing or invalid JSON parsed',
        'IP now allowed' => 'IP is not allowed',
    ];
?>