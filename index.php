<?php
    //include settings
    include_once 'settings.php';

    //paths
    define('PATH_SITE', ''); //define sites url path. Example: if example.com/test/ use /test. Also update DATA_PATH in setting.php
    define('PATH_APP', __DIR__.'/app');
    define('PATH_SERVICES', __DIR__.'/services');

    //if in dev mode
    if(IN_DEV_MODE){
        //error reporting
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    //https is not enabled 
    if(!IN_DEV_MODE && $_SERVER['HTTPS']){
        http_response_code(404);
        die('Error: HTTPS is not enabled');
    }

    //load app
    if(file_exists(PATH_APP.'/app.php')){
        include_once PATH_APP.'/app.php';
    }

    //kill app
    die;
?>